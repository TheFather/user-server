package ru.madela.pattern.userserver.util;

import java.util.Locale;

public final class KnuthMorrisPrattAlgorithmSearchUtil {

    private KnuthMorrisPrattAlgorithmSearchUtil() {

    }

    public static boolean isSearch(String scouted, String target) {
        String scoutedLow = scouted.toLowerCase(Locale.ROOT);
        String targetLow = target.toLowerCase(Locale.ROOT);
        int[] prefixFunc = prefixFunction(target);
        int scoutedPos = 0;
        int targetPos = 0;
        while (scoutedPos < scoutedLow.length()) {
            if (targetLow.charAt(targetPos) == scoutedLow.charAt(scoutedPos)) {
                targetPos++;
                scoutedPos++;
            }
            if (targetPos == targetLow.length()) {
                return true;
            }
            if (scoutedPos < scoutedLow.length() && targetLow.charAt(targetPos) != scoutedLow.charAt(scoutedPos)) {
                boolean flag = targetPos != 0;
                targetPos = flag ? prefixFunc[targetPos - 1] : targetPos;
                scoutedPos = !flag ? scoutedPos + 1 : scoutedPos;
            }
        }
        return false;
    }

    private static int[] prefixFunction(String target) {
        int[] prefix = new int[target.length()];
        for (int i = 1; i < target.length(); i++) {
            int j = 0;
            while (i + j < target.length() && target.charAt(j) == target.charAt(i + j)) {
                prefix[i + j] = Math.max(prefix[i + j], j + 1);
                j++;
            }
        }
        return prefix;
    }
}
