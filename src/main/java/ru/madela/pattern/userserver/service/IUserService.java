package ru.madela.pattern.userserver.service;

import ru.madela.pattern.userserver.dto.UserDto;
import ru.madela.pattern.userserver.exception.UserNotFoundException;

import java.util.List;

public interface IUserService {
    List<UserDto> getAllUsers(int page) throws UserNotFoundException;

    List<UserDto> getUsersByNameOrSurname(String target, int page) throws UserNotFoundException;

    boolean addUser(UserDto user);
}
