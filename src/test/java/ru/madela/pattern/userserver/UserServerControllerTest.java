package ru.madela.pattern.userserver;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(value = "/application-test.yml")
class UserServerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getUserTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("http://localhost:8080/user/get/ohn/1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("John")))
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("Johnson")));
    }

    @Test
    public void getAllUsersTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("http://localhost:8080/user/all/1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getUserNegativePageTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("http://localhost:8080/user/get/julia/-1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.content()
                        .string(Matchers.containsString("User list not found")))
                .andExpect(MockMvcResultMatchers.content()
                        .string(Matchers.containsString("Internal Server Error")))
                .andExpect(MockMvcResultMatchers.content()
                        .string(Matchers.containsString("500")));
    }

    @Test
    public void getAllUsersNegativePageTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("http://localhost:8080/user/all/-1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.content()
                        .string(Matchers.containsString("User list not found")))
                .andExpect(MockMvcResultMatchers.content()
                        .string(Matchers.containsString("Internal Server Error")))
                .andExpect(MockMvcResultMatchers.content()
                        .string(Matchers.containsString("500")));
    }
}
