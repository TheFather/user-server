package ru.madela.pattern.userserver;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.madela.pattern.userserver.util.KnuthMorrisPrattAlgorithmSearchUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class KnuthMorrisPrattAlgorithmSearchUtilTest {

    @Test
    public void searchFoundTest() {
        boolean isFound = KnuthMorrisPrattAlgorithmSearchUtil.isSearch("John", "hn");
        Assertions.assertTrue(isFound);
    }

    @Test
    public void searchFoundTestDifferentCaseTest() {
        boolean isFound = KnuthMorrisPrattAlgorithmSearchUtil.isSearch("John", "Hn");
        Assertions.assertTrue(isFound);
    }
}
